import { useState } from 'react'

/**
  * useNavbar provides state logic for general Navbars
  *
  * Param itemNames: array - List of Navbar names
  * Returns: {
  *   activeItem: integer - currently active panel ID
  *   itemNames: array - List of Navbar names
  *   changePanel: Function to pass to Navbar for switching active Panel
  * }
  */

export default function useNavbar (itemNames) {
  const [activeItem, setActiveItem] = useState(0)

  const changePanel = (panel) => {
    /**
     * Param: Navbar item
     * Returns: null
     */
    setActiveItem(panel.itemId)
  }

  return [activeItem, itemNames, changePanel]
}
