import { useState } from 'react'

export default function useSearchField (onSearch = (value) => {}) {
  const [value, setValue] = useState(undefined)

  const handleChange = (val) => {
    setValue(val)
  }

  const handleSearch = () => {
    onSearch(value)
    setValue(undefined)
  }

  const clearSearch = () => {
    setValue(undefined)
  }

  return [value, handleChange, handleSearch, clearSearch]
}
