import { useState } from 'react'
import useTypeaheadSelectInput from './useTypeaheadSelectInput'

/**
 * useConfigSelection contains the stateful logic for the ConfigSelection
 * component.
 *
 * In the upload state, the configData variable contains the payload of the
 * config and the configName variable the config name. In the selection state,
 * the configData variable is ='', while the configName contains the name of
 * the currently selected option.
 *
 * Params:
 *  initUpload: boolean - Config to be uploaded initally?
 *  initConfigData: string - Default config data
 *  initConfigName: string - Default config name
 *  onConfigChange: function - Optional callback on change of config data/name
 *  onUploadChange: function - Optional callback on change of upload option
 *
 * Returns:
 *  [
 *    upload: boolean - Config to be uploaded?
 *    configData: string - Current config data
 *    configName: string - Current config name
 *    handleConfigChange: function - To be passed to ConfigSelection
 *    handleUploadChange: function - To be passed to ConfigSelection
 *    resetData: function - Manually change current config Data
 *  ]
 */
export default function useConfigSelection (
  initUpload = true,
  initConfigData = '',
  initConfigName = '',
  onConfigChange = (data, name) => {},
  onUploadChange = (upload) => {}
) {
  const [upload, setUpload] = useState(initUpload)
  const [configData, setConfigData] = useState(initConfigData)
  const [configName, setConfigName] = useState(initConfigName)

  const handleConfigChange = (data, name) => {
    setConfigData(data)
    setConfigName(name)
    onConfigChange(data, name)
  }

  const handleUploadChange = () => {
    const curUpload = upload
    onUploadChange(!curUpload)
    onConfigChange('', '')
    setUpload(!upload)
    setConfigName('')
    setConfigData('')
  }

  const resetData = (name = '', data = '', upload = true) => {
    handleConfigChange(data, name)
    setUpload(upload)
  }

  const [
    // eslint-disable-next-line no-unused-vars
    selected,
    isSelectOpen,
    onSelect,
    onToggle,
    clearSelection
  ] = useTypeaheadSelectInput(
    configName,
    (name) => handleConfigChange('', name)
  )

  return [
    upload,
    configData,
    configName,
    handleConfigChange,
    handleUploadChange,
    isSelectOpen,
    onSelect,
    onToggle,
    clearSelection,
    resetData
  ]
}
