import { useState } from 'react'

/**
 * useButtonedModal hook provides a way to read and update modal visibility
 *
 * Params:
 *  isOpen: boolean - Sets initial modal state
 *
 * Returns:
 * {
 *  isModalOpen: boolean that indicates if modal is open
 *  toggleModal: function that toggles the modal open and close
 *  closeModal: function that closes modal
 * }
 */

export default function useButtonedModal (isOpen = false) {
  const [isModalOpen, setIsModalOpen] = useState(isOpen)

  const toggleModal = () => {
    setIsModalOpen(!isModalOpen)
  }

  const closeModal = () => {
    setIsModalOpen(false)
  }

  return [
    isModalOpen,
    toggleModal,
    closeModal
  ]
}
