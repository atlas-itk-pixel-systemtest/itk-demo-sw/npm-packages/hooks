import { useState } from 'react'

/**
 * useCheckedTextInput provides the stateful logic for the CheckedTextInput
 * component.
 *
 * Params:
 *  dafaultString: string - Default field value. Undefined for empty field.
 *  onChange: function - Callback to be called when input value changes
 *
 * Returns:
 * [
 *  value: string - Value to be given as prop to CheckedTextInput
 *  handleChange: function - Handles change of CheckedTextInput field
 * ]
 */

export default function useCheckedTextInput (
  defaultString = undefined,
  onChange = (val) => {}
) {
  const [value, setValue] = useState(defaultString)

  const handleChange = (val) => {
    setValue(val)
    onChange(val)
  }

  return [value, handleChange]
}
