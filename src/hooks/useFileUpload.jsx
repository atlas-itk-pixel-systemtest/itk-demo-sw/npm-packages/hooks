import { useState } from 'react'

/**
 * useFileUpload defines the stateful logic of the FileUpload component from
 * Patternfly.
 *
 * Params:
 *  initialFilename: string - Initial filename to be depictes
 *  initialData: string - Initial upload file data
 *
 * Returns:
 * [
 *  filename: string - Current value of the filename
 *  data: string - Current value of the file data
 *  handleChange: function - Handles upload and change of files
 * ]
 */

export default function useFileUpload
(
  initialFilename = '',
  initialData = ''
) {
  const [filename, setFilename] = useState(initialFilename)
  const [data, setData] = useState(initialData)

  const handleChange = (value, name) => {
    setData(value)
    setFilename(name)
  }

  return [filename, data, handleChange]
}
