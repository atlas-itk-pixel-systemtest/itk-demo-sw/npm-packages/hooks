import { useState } from 'react'

/**
 * useStandardTable defines the stateful logic of the StandardTable
 * component.
 *
 * Params:
 *  initialRows: array - Initial set of rows to be depicted
 *
 * Returns:
 * [
 *  rows: array - Current set of rows
 *  setRows: function - Changes row content
 * ]
 */

export default function useStandardTable
(
  initialRows = []
) {
  const [rows, setRows] = useState(initialRows)

  return [rows, setRows]
}
