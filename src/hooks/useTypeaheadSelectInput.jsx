import { useState } from 'react'

/**
 * useTypeaheadSelectInput provides the stateful logic for the basic
 * TypeaheadSelectInput component.
 *
 * Note: This hook only implements the basic logic of selecting and deselecting
 * options. Further logic for persistent text input needs to be implemented
 * manually. You can use the 'onSelect' parameter for this.
 *
 * Params:
 *  initialSelect: string - Initally selected option
 *  onSelect: function - Called on selection of an option
 *
 * Returns:
 * [
 *  selected: string - Currently selected option
 *  isOpen: boolean - Is Select open?
 *  handleSelect: function - Callback to handle the selection of an option
 *  handleToggle: function - Callback to handle opening/closing of Select
 *  clearSelection: function - Clears selection and closes Select
 * ]
 */

export default function useTypeaheadSelectInput (
  initialSelect = null,
  callbackSelect = (selection) => {}
) {
  const [selected, setSelected] = useState(initialSelect)
  const [isOpen, setIsOpen] = useState(false)

  const onToggle = () => setIsOpen(!isOpen)

  const clearSelection = () => {
    setSelected(null)
    callbackSelect(null)
    setIsOpen(false)
  }

  const onSelect = (event, selection, isPlaceholder) => {
    if (isPlaceholder) {
      clearSelection()
    } else {
      setSelected(selection)
      setIsOpen(false)
      callbackSelect(selection)
    }
  }

  return [selected, isOpen, onSelect, onToggle, clearSelection]
}
