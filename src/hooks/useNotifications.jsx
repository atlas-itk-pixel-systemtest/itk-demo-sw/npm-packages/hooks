import { useState } from 'react'

/**
 * useNotifications provides the stateful logic of notifications, to be used
 * with the 'Notifications' component
 *
 * The format of errors that are passed to the onError function needs to
 * be compatible with the RCF7807 standard. The other callback functions only
 * take single strings as arguments.
 *
 * Param onLog: function - Called after every addition of a new alert. Required
 *                         format: (string: logLevel, string: message) => null
 * Returns: [
 *  alerts: array - alert objects in a format compatible with the
 *          'Notifications' component
 *  onError: function - Adds error-level alert (RCF7807)
 *  onGenericError: function - Adds error-level alert
 *  onWarn: function - Adds warning-level alert
 *  onInfo: function - Adds info-level alert
 *  onSuccess: function - Adds success-level alert
 *  deleteAlert: function - Deletes a given alert from the list
 * ]
 */

const useNotifications = (onLog = () => { }) => {
  const [alerts, setAlerts] = useState([])

  const addAlert = (log) => {
    const tmpAlerts = alerts
    tmpAlerts.push(log)
    setAlerts([...tmpAlerts])
  }

  const deleteAlert = (i) => {
    /**
     * Param: integer - Index of alert to be deleted
     * Returns: null
     */
    const tmpAlerts = alerts
    tmpAlerts.splice(i, 1, undefined)
    setAlerts([...tmpAlerts])
  }

  const onError = (err, timeout = false) => {
    /**
     * Param: object - Format (options marked with * are required):
     *    {
     *      status*: integer - HTTP statuscode
     *      title*: string - Error title
     *      detail*: string - Error description
     *      type: string - URI for further information on the error
     *      instance: string - Location of the occurrence of the error
     *    }
     * Returns: null
     */

    const requiredKeys = ['status', 'title', 'detail']
    if (!requiredKeys.every(key => Object.keys(err).includes(key))) {
      const msg = 'An unspecified error occurred. Please consult a developer.'
      addAlert({
        status: 500,
        title: 'Unknown Error',
        message: 'An unspecified error occurred. Please consult a developer.',
        variant: 'danger',
        timeout: timeout
      })
      onLog('error', msg)
      return
    }
    const message = {
      main: err.detail
    }
    const optionalOptions = ['type', 'instance']
    for (const opt of optionalOptions) {
      if (Object.keys(err).includes(opt)) {
        message[opt] = err[opt]
      }
    }
    addAlert({
      status: err.status,
      title: err.title,
      message: message,
      variant: 'danger',
      timeout: timeout
    })
    onLog('error', message.main)
  }

  const onGenericError = (code, detail, message, timeout = false) => {
    /**
     * Param: object - Format (options marked with * are required):
     * code/name*: string - HTTP statuscode or name
     * detail*: string - Error title
     * message*: string - Error description
     * Returns: null
     */
    addAlert({
      status: code,
      title: detail,
      message: message,
      variant: 'danger',
      timeout: timeout
    })
    onLog('error', message)
  }

  const onInfo = (msg, timeout = false) => {
    /**
     * Param: string - alert message
     * Returns: null
     */
    addAlert({
      title: msg,
      variant: 'info',
      timeout: timeout
    })
    onLog('info', msg)
  }

  const onWarn = (msg, timeout = false) => {
    /**
     * Param: string - alert message
     * Returns: null
     */
    addAlert({
      title: msg,
      variant: 'warning',
      timeout: timeout
    })
    onLog('warning', msg)
  }

  const onSuccess = (msg, timeout = false) => {
    /**
     * Param: string - alert message
     * Returns: null
     */
    addAlert({
      title: msg,
      variant: 'success',
      timeout: timeout
    })
    onLog('info', msg)
  }

  return [alerts, onError, onWarn, onInfo, onSuccess, deleteAlert, onGenericError]
}

export default useNotifications
