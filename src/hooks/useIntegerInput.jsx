import { useState } from 'react'

/**
 * useIntegerInput provides the stateful logic for the IntegerInput component.
 *
 * Params:
 *  defaultInt: integer - Default field value. Undefined for empty field.
 *  onChange: function - Callback to be called when input value changes
 *
 * Returns:
 * [
 *  value: integer - Value to be given as prop to IntegerInput
 *  handleChange: function - Handles change of IntegerInput field
 * ]
 */

export default function useIntegerInput (
  defaultInt = undefined,
  onChange = (val) => {}
) {
  const [value, setValue] = useState(defaultInt)

  const handleChange = (val) => {
    setValue(val)
    onChange(val)
  }

  return [value, handleChange]
}
