import { useState } from 'react'

/**
 * usePagination hook provides a way to handle the Patternfly Pagination
 * component.
 *
 * Params:
 *  initPerPage: integer - Initial number of items per page
 *
 * Returns:
 * {
 *  page: integer - Current page
 *  perPage: integer - Current number of items per page
 *  handleSetPage: function - Handles setting of new page
 *  handlePerPageSelect: function - Handles change of number of items per page
 *  resetPage: function - Resets current page to 1 (or a specified value)
 * }
 */

export default function usePagination (
  initPerPage = 0
) {
  const [page, setPage] = useState(1)
  const [perPage, setPerPage] = useState(initPerPage)

  const handleSetPage = (_evt, newPage, perPage) => {
    setPage(newPage)
    setPerPage(perPage)
  }

  const handlePerPageSelect = (_evt, newPerPage, newPage = 1) => {
    setPerPage(newPerPage)
    setPage(newPage)
  }

  const resetPage = (val = 1) => {
    setPage(val)
  }

  return [page, perPage, handleSetPage, handlePerPageSelect, resetPage]
}
