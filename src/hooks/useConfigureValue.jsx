import { useState, useEffect } from 'react'
import { generalRequest, registryInterface } from '@itk-demo-sw/utility-functions'

const useConfigureValue = (
  configEndpoint,
  keyName,
  defaultValue = '',
  fallbackConfigKey = ''
) => {
  const [value, setValue] = useState(defaultValue)
  useEffect(
    () => {
      generalRequest(configEndpoint).then(
        data => {
          if (fallbackConfigKey in data && data[fallbackConfigKey] !== '') {
            setValue(data[fallbackConfigKey])
          } else if (
            ('srUrl' in data) && (keyName in data) &&
            (data.srUrl !== '') && (data[keyName] !== '')
          ) {
            const fullKey = `${data[keyName]}`
            const sr = registryInterface(data.srUrl)
            const watcher = sr.watch([fullKey])
            watcher.addEventListener(fullKey, (ev) => {
              setValue(ev.data)
            })
          }
        }
      ).catch(
        err => console.log(err)
      )
    }, [keyName, fallbackConfigKey, configEndpoint]
  )
  return value
}

export default useConfigureValue
