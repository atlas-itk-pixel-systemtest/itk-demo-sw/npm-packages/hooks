import { useState } from 'react'

/**
  * useLoggingViewer provides the general stateful logic for the LoggingViewer
  *
  * Returns: {
  *   content: array - List of log objects
  *   addLog: function - To be called when adding a log message
  * }
  */

export default function useLoggingViewer () {
  const [content, setContent] = useState([])

  const addLog = (level, log) => {
    /**
     * Params: {
     *  level: string - log-level of the message
     *  log: string - actual message
     * }
     */
    setContent(
      (prevLogContent) => prevLogContent.concat([{ lvl: level, msg: log }])
    )
  }
  return [content, addLog]
}
