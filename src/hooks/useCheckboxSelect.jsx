import { useState } from 'react'

/**
 * useCheckboxSelect provides the stateful logic for the CheckboxSelect
 * component
 *
 * Returns:
 * [
 *  isOpen: boolean - Is Select currently opened
 *  selections: array - Currently selected options
 *  onToggle: function - Callback for toggeling Select component
 *  onSelect: function - Callback for (de-)selecting an option
 *  clearSelection: function - Deselect all options
 * ]
 */

export default function useCheckboxSelect () {
  const [isOpen, setIsOpen] = useState(false)
  const [selections, setSelections] = useState([])

  const onToggle = () => setIsOpen(!isOpen)

  const onSelect = (event, selection) => {
    setSelections(prevSelections => {
      return selections.includes(selection)
        ? prevSelections.filter(value => value !== selection)
        : [...prevSelections, selection]
    })
  }

  const clearSelections = () => {
    setSelections([])
    setIsOpen(false)
  }

  return [isOpen, selections, onToggle, onSelect, clearSelections]
}
