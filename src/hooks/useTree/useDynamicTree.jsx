/**
 * Extension to the `useTree` hook. It schould be used by adding 'dynamic' to the `feature` input of the `useTree` hook.
 *
 * This hook provides utility functions to dynamically add sub trees.
 *
 * @returns `{ addSubTree, hasComponentChildren }`
 */
const useDynamicTree = (setTree, setMetaData, setExpandedMap, findParentTrees, buildTree, tree, findId) => {
  const addSubTree = (rawTreeData) => {
    const id = rawTreeData.id
    const { tree, metaData, expandedMap } = buildTree(rawTreeData)
    // console.log('adding Tree with id', id, rawTreeData, newTree, newMeta, dest)
    setTree((t) => {
      const curTree = { ...t }
      const parTrees = findParentTrees(curTree, id)
      parTrees.forEach(parTree => {
        parTree[id] = { ...tree[id], ...parTree[id] }
      })
      return curTree
    })
    setMetaData((m) => {
      return { ...m, ...metaData }
    })
    setExpandedMap((e) => {
      return { ...expandedMap, ...e }
    })
  }

  const hasComponentChildren = (id) => {
    const subTree = findId(tree, id)[0]
    return !Object.values(subTree).every(child => child === null)
  }

  return { addSubTree, hasComponentChildren }
}

export default useDynamicTree
