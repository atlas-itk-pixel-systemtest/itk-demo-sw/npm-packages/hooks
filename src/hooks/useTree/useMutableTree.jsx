/**
 * Extension to the `useTree` hook. It schould be used by adding 'mutable' to the `feature` input of the `useTree` hook.
 *
 * This hook provides utility functions to mutate the tree entries.
 *
 * @returns `{ addChild, changeId, deleteComponent }`
 */
const useMutableTree = (setTree, setMetaData, setExpandedMap, findParentTrees, findId) => {
  const addChild = (parentId, childId, childMetaData, child = {}) => {
    setTree((t) => {
      const tree = { ...t }
      const parTrees = findId(tree, parentId)
      console.log('addChild: to ', parentId, ' found ', parTrees.length, ' parentTrees')
      parTrees.forEach(parTree => {
        parTree[childId] = child
      })
      console.log('addCild: ', childId, ' resulted in ', tree)
      return tree
    })
    setMetaData((m) => {
      const meta = { ...m, [childId]: childMetaData }
      return meta
    })
    setExpandedMap((e) => {
      const exp = { ...e }
      exp[childId] = true
      return exp
    })
  }

  const changeId = (oldId, newId) => {
    setTree((t) => {
      const tree = { ...t }
      const parTrees = findParentTrees(tree, oldId)
      parTrees.forEach(parTree => {
        parTree[newId] = parTree[oldId]
        delete parTree[oldId]
      })
      return tree
    })
    setMetaData((m) => {
      const meta = { ...m }
      meta[newId] = meta[oldId]
      delete meta[oldId]
      return meta
    })
    setExpandedMap((e) => {
      const exp = { ...e }
      exp[newId] = exp[oldId]
      delete exp[oldId]
      return exp
    })
  }

  const deleteComponent = (id) => {
    setTree((t) => {
      const tree = { ...t }
      const parTrees = findParentTrees(tree, id)
      parTrees.forEach(parTree => delete parTree[id])
      return tree
    })
    setMetaData((m) => {
      const meta = { ...m }
      delete meta[id]
      return meta
    })
    setExpandedMap((e) => {
      const exp = { ...e }
      delete exp[id]
      return exp
    })
  }

  return { addChild, changeId, deleteComponent }
}

export default useMutableTree
