# hooks

Common React hooks for GUIs of itk-demo microservices. 

## How to change hooks and publish to private registry

A quick quide to how individual hooks can be adapted and others can be added. 
**Make sure that any changes to existing hooks are backwards compatible!**

### Checking out the git repository
The following explanation assumes that you cloned the repository into
```
mylocal-itk-demo-sw/npm-packages/hooks/
```
And test the changes with the local project living in
```
mylocal-itk-demo-sw/itk-demo-template/
```
### Building
After you have coded your changes you can build them using:
```
cd mylocal-itk-demo-sw/npm-packages/hooks/
npm i
npm run build
```
The built React hooks are then stored in the `build/`directory in the project's root folder. 
Before you push your changes to gitlab or publish them to the package registry, you can import them into a local project. This allows you to test your changes and avoid messing up the public registry. Install the locally built hooks from your project as follows: 
```
cd mylocal-itk-demo-sw/itk-demo-template/ui
npm i ../../npm-packages/hooks/
npm link ../../npm-packages/hooks/node_modules/react/
```
*Note: The linking of react is necessary to avoid having multiple versions of React running, which would result in errors. Best reinstall all packages in the testing project after you have validated your hooks.*
You can access the local hooks via `import { ... } from '@itk_demo_sw/hooks'`. To check that you are actually using your local changes, look at the local `node_modules` folder in your testing project. If the `@itk_demo_sw/hooks` entry is a symbolic link to your local installation of this repository, everything worked fine.
Once you are sure that your changes work as expected you have to commit your changes to git. Then you can continue publishing them to the package registry. 

### Publishing
**Note: This still has to be done manually!** Eventually gitlab CI should automatically integrate changes to hooks. For now this has not yet been implemented. 

To publish first change the version of your package. You have three choices of version incrementing: major, minor and patch. Use 'minor' whenever you add a new hook or make larger changes to an existing one. Use 'patch' whenever you make small changes to an existing hook or fix a bug. Never use 'major'.
The incrementing can by done via:
```
npm version [minor | patch]
```
Once you have done this, publish your changes to the package registry using:
```
npm publish
```
Now all your changes should be available in the registry.

## Migrating to the new registryInterface in React GUIs

The standard GUIs only use the `registryInterface.watch` function indirectly through the
`@itk-demo-sw/hooks/useConfigureValue` function. But `useConfigureValue` uses some environment variables that need to be updated. The following sections show what files must be changed.

### compose.yaml

replace
```
    environment:
      - BACKEND_URL=${UI_BACKEND_URL}
      - ETCD_HOST=${HOST}
      - ETCD_PORT=2379
      - ETCD_NAMESPACE=demi
      - API_URL_KEY=${API_URL_KEY}
```
with
```
    environment:
      - DEFAULT_BACKEND_URL=${DEFAULT_BACKEND_URL}
      - SR_URL=http://${HOST}:5111/api
      - API_URL_KEY=${API_URL_KEY}
```

### Dockerfile

replace
```
CMD envsubst \
    '${ETCD_HOST},${ETCD_PORT},${ETCD_NAMESPACE},${API_URL_KEY},${BACKEND_URL}'\
    < /etc/nginx/conf.d/nginx.conf.template > /etc/nginx/nginx.conf &&\
    nginx -g 'daemon off;'
```
with
```
CMD envsubst \
    '${SR_URL}$,${API_URL_KEY},${DEFAULT_BACKEND_URL}'\
    < /etc/nginx/conf.d/nginx.conf.template > /etc/nginx/nginx.conf &&\
    nginx -g 'daemon off;'
```

### nginx.conf.template
replace
```
        location /config {
            default_type application/json;
            return 200 '{
                "status": 200, 
                "etcdHost":"${ETCD_HOST}",
                "etcdPort":"${ETCD_PORT}",
                "etcdNamespace":"${ETCD_NAMESPACE}",
                "urlKey":"${API_URL_KEY}",
                "backendUrl":"${BACKEND_URL}"
            }';
        }
```
with
```
        location /config {
            default_type application/json;
            return 200 '{
                "status": 200, 
                "srUrl":"${SR_URL}",
                "urlKey":"${API_URL_KEY}",
                "backendUrl":"${DEFAULT_BACKEND_URL}"
            }';
        }
```

### package.json
 set hooks package version
 ```
    "@itk-demo-sw/hooks": "^4.1.0",
 ```

## Display ApiUrl as constant string in admin panel

The internal changes of the `@itk-demo-sw/hooks/useConfigureValue` function allow to display the currently used Url for the backend api in the admin panel. This requires some changes to the React components.

### src/screens/dashboard.jsx

replace
```
  const { getConfig } = useConfig()
  const ApiUrl = useConfigureValue('/config', 'urlKey', getConfig('backend'), 'backendUrl')
```
with
```
  const { getConfig, setConfig } = useConfig()
  const ApiUrl = useConfigureValue('/config', 'urlKey', getConfig('backend'), 'backendUrl')
  useEffect(() => {
    setConfig('backend', dbApiUrl)
  }, [dbApiUrl])
```

### src/admin-panel/config.jsx
replace
```
    backend: {
      type: 'string',
      title: 'Backend URL/ prefix',
      description: 'Backend URL/ prefix',
      default: process.env.REACT_APP_CONFIGDB_BACKEND_URL || '/api'
    }
```
with
```
    backend: {
      type: 'custom',
      title: 'Backend URL/ prefix',
      description: 'Backend URL/ prefix',
      default: process.env.REACT_APP_CONFIGDB_BACKEND_URL || '/api',
      parser: (value) => {
        if (typeof value === 'string') {
          return value
        } else {
          throw new Error('Invalid backend URL')
        }
      }
    }
```

### src/admin-panel/panel-badge.jsx
replace
```
              const setting = field.type === 'boolean'
                ? (
                  <Checkbox
                    id={'checkboxid' + field.key}
                    key={'checkbox' + field.key}
                    label={field.key}
                    isChecked={field.value}
                    onChange={(val, evt) => field.set(val)}
                  />
                  )
                : (
                  <TextInput
                    key={'textinput' + field.key}
                    value={field.value}
                    label={field.key}
                    onChange={field.set}
                    aria-label={'TextInput ' + field.key}
                  />
                  )
```
with
```
              let setting = <></>
              switch (field.type) {
                case 'boolean':
                  setting =
                    <Checkbox
                      id={'checkboxid' + field.key}
                      key={'checkbox' + field.key}
                      label={field.key}
                      isChecked={field.value}
                      onChange={(val, evt) => field.set(val)}
                    />
                  break
                case 'custom':
                  setting = <Text
                    key={'text' + field.key}>
                    {field.value}
                  </Text>
                  break
                default:
                  setting =
                    <TextInput
                      key={'textinput' + field.key}
                      value={field.value}
                      label={field.key}
                      onChange={field.set}
                      aria-label={'TextInput ' + field.key}
                    />
              }
```
